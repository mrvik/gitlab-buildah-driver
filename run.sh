#!/bin/bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${currentDir}/common.sh"
set -e

echo "Running script inside container"

shell=$(buildah run -- "$CONTAINER_ID" sh -c 'which bash||which ash||which sh||echo $SHELL')
echo "Found shell $shell"

buildah run -- "$CONTAINER_ID" "$shell" < "${1}"
exitCode=$?

case $exitCode in
    0)
        echo "Command run successfully"
        ;;
    125 | 126)
        echo "Error on OCI runtime. Code $exitCode"
        ;;
    *)
        echo "Error inside container. Code $exitCode"
        exit $BUILD_FAILURE_EXIT_CODE
esac

exit $exitCode
