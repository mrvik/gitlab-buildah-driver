#!/bin/bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${currentDir}/common.sh"
set -e

trap "exit $SYSTEM_FAILURE_EXIT_CODE" ERR

prepare_container(){
    if buildah inspect "$CONTAINER_ID" &>/dev/null; then
        echo "Removing stale container"
        buildah rm -f "$CONTAINER_ID" # Remove old (stale) container
    fi
    local rawImageName="$(echo "$CUSTOM_ENV_CI_JOB_IMAGE"|sed -E 's/\$/\$CUSTOM_ENV_/g')"
    local image="$(eval echo -e "$rawImageName")"
    local container="$CONTAINER_ID"
    echo "Pulling image $image"
    buildah pull "$image"
    buildah from --name "$CONTAINER_ID" "$image"

    # Install gitlab-runner binary since we need for cache/artifacts.
    echo "Installing GitLab dependencies on base image"
    buildah add --chown 0:0 "$container" "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64" "/usr/local/bin/gitlab-runner"
    buildah run --user 0:0 -- "$container" chmod +x /usr/local/bin/gitlab-runner
    buildah run --user 0:0 -- "$container" sh -c "which git || (which apk && apk --update add git) || (which apt && apt update && apt install git)"
    buildah run --user 0:0 -- "$container" sh -c "mkdir -p /var/lib/gitlab-runner && chmod 777 /var/lib/gitlab-runner"
}

prepare_container
