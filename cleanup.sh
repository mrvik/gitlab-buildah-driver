#!/bin/bash

currentDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
source "${currentDir}/common.sh"

echo "Doing cleanup"

buildah inspect "$CONTAINER_ID" &>/dev/null && buildah rm "$CONTAINER_ID"
