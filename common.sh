#!/bin/bash

CONTAINER_ID="runner-$CUSTOM_ENV_CI_RUNNER_ID-project-$CUSTOM_ENV_CI_PROJECT_ID-concurrent-$CUSTOM_ENV_CI_CONCURRENT_PROJECT_ID-$CUSTOM_ENV_CI_JOB_ID"
DIRTY_IMAGE_NAME="${CUSTOM_ENV_CI_JOB_IMAGE}__dirty"
