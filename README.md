# GitLab runner driver for `buildah`

This driver was intended to be a `podman` driver but uses `buildah` instead of `podman` for ease of use (and avoid injecting a pause as `PID 1`)

## Limitations/Caveats

* Container shell is detected automatically in the order of preference `bash -> ash -> sh`.
* Host machine (the one with `gitlab-runner`) must have `buildah` installed and working on it. Also, the user running `gitlab-runner` service must be able to use `buildah`.
* Obviously only `Linux` machines are supported.
* Base image is poisoned with `gitlab-runner` binary. It isn't intrusive at all (unless you have another `gitlab-runner` binary laying around inside the image), but now you know it.
* Container is removed on the `cleanup` step but images aren't (really nothing gets commited). You may want to check it out with
  * `buildah containers` - List all containers
  * `buildah rm --all` - Remove all `buildah` containers
  * `buildah rmi --all` - Remove all `buildah` images
* Fresh images are always pulled, if you thing cache is causing problems, do `buildah rmi --all` with the user running `gitlab-runner`
* `gitlab-runner` binary image URL is hardcoded. If you'll use this driver into a non-x86_64 machine, open an issue or a MR.
* Services are not supported. `podman` inside `podman` is not supported unless you do [tricky things](https://github.com/containers/podman/issues/3917)

## Bugs
You may find a lot of bugs. Fill an Issue or try to fix it and make an MR if you find one.
